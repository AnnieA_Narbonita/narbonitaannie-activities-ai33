var users = [
    {
        id: 1,
        name: "June",
       

        middlename: "Dublan",
        lastname: "Sidon",
        email: "cesar_sidon@yahoo.com",
        date: "January 1, 2020",

    },
    {
        id: 2,
        name: "Snor",
        middlename: "Dublan",
          email: "cesarsidon666@gmail.com",
        lastname: "Sidon",
        date: "December 25, 2020",
        
    },
	{
        id: 3,
        name: "Tao",
        email: "sidoncesar@gmail.com",
        date: "November 1, 2020",
        middlename: "Dublan",
        lastname: "Sidon",
        
    }
];

$("#overlay").hide();



$("#kape").click(function(){
    $("#overlay").toggle();
});

$("#closed").click(function(){
    $("#overlay").toggle();
});
$("#kope").click(function(){
    $("#overlay").toggle();
});

$.each(users, function (i, user) {
    appendToUsrTable(user);
});

$("form").submit(function (e) {
    e.preventDefault();
});
function deleteUser(id) {
    var action = confirm("Are you sure?");
    var msg = "Contact deleted successfully!";
    users.forEach(function (user, i) {
        if (user.id == id && action != false) {
            users.splice(i, 1);
            $("#userTable #user-" + user.id).remove();
            flashMessage(msg);
        }
    });
}


function flashMessage(msg) {
    $(".flashMsg").remove();
    $(".row").prepend(`
         
      `);
}

function appendToUsrTable(user) {
    $("#userTable > tbody:last-child").append(`
          <tr id="user-${user.id}" class="text-center">
              <td class="userData" name="name">${user.name}   ${user.middlename}  ${user.lastname}</td>
              '<td class="userData" name="email">${user.email} </td>
               '<td class="userData" name="email">${user.date}</td>
               '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/edit.png" height="32" width="32" onClick="deleteUser(${user.id})"></a>
              </td>
              '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/delete.png" height="32" width="32" onClick="deleteUser(${user.id})"</a>
              </td>
             
          </tr>
      `);
}
